import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerList from './Sales/CustomerList';
import SalespersonList from './Sales/SalespersonList';
import SalesList from './Sales/SalesList';
import CustomerForm from './Sales/CustomerForm';
import SalespersonForm from './Sales/SalespersonForm';
import SalesForm from './Sales/SalesForm';
import SalesHistory from './Sales/SalesHistory';
import ServiceHistory from './Services/ServiceHistory'
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelForm from './Inventory/ModelForm';
import AutomobileForm from './Services/AutomobileForm';
import ManufacturerList from "./Inventory/ManufactuererList";
import AutomobileList from './Services/AutomobileList';
import ModelList from "./Inventory/ModelsList";
import TechnicianList from "./Services/TechnicianList";
import TechnicianForm from "./Services/TechnicianForm";
import AppointmentForm from "./Services/AppointmentForm";
import AppointmentList from "./Services/AppointmentList";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturer/list/" element={<ManufacturerList />} />
          <Route path="customers/list/" element={<CustomerList />} />
          <Route path="salespeople/list/" element={<SalespersonList />} />
          <Route path="sales/list/" element={<SalesList />} />
          <Route path="customer/create/" element={<CustomerForm />} />
          <Route path="salespeople/create/" element={<SalespersonForm />} />
          <Route path="sales/create/" element={<SalesForm />} />
          <Route path="sales/history/" element={<SalesHistory />} />
          <Route path="appointments/history/" element={<ServiceHistory />} />
          <Route path="manufacturer/create/" element={<ManufacturerForm />} />
          <Route path="models/create/" element={<ModelForm />} />
          <Route path="models/list/" element={<ModelList />} />
          <Route path="automobile/create/" element={<AutomobileForm />} />
          <Route path="automobile/list/" element={<AutomobileList />} />
          <Route path="technicians/list/" element={<TechnicianList />} />
          <Route path="technicians/create/" element={<TechnicianForm />} />
          <Route path="appointments/list/" element={<AppointmentList />} />
          <Route path="appointments/create/" element={<AppointmentForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
