import React, { useEffect, useState } from "react";

function SalesForm() {
  const [autos, setAutos] = useState([]);
  const [automobile, setAuto] = useState("");
  const [salespeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState("");
  const [customers, setCustomers] = useState([]);
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");

  const fetchAuto = async () => {
    const autoUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(autoUrl);
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  };

  const fetchSalesPeople = async () => {
    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespeopleUrl);
    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.salespeople);
    }
  };

  const fetchCustomer = async () => {
    const customerUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customerUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchAuto();
    fetchSalesPeople();
    fetchCustomer();
  }, []);

  const handleAutoChange = (event) => {
    const value = event.target.value;
    setAuto(value);
  };
  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      automobile: automobile,
      salesperson: salesPerson,
      customer: customer,
      price: price,
    };

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const autoUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
      const fetchSoldConfig = {
        method: "put",
        body: JSON.stringify({ sold: true }),
        headers: {
          "Content-Type": "application/json",
        },
      };
      await fetch(autoUrl, fetchSoldConfig);
      setAuto("");
      setSalesPerson("");
      setCustomer("");
      setPrice("");
    }
  };
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create A New Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select value={automobile} required onChange={handleAutoChange} name="auto" id="auto" className="form-select">
                  <option value="">Choose an automobile VIN</option>
                  {autos.filter((auto) => auto.sold === false).map((auto) => {
                      return (
                        <option key={auto.id} value={auto.vin}>
                          {auto.vin}
                        </option>
                      );
                    })}
                </select>
              </div>
              <div className="mb-3">
                <select value={salesPerson} required onChange={handleSalesPersonChange} name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a sales person</option>
                  {salespeople.map((saleperson) => {
                    return (
                      <option key={saleperson.employee_id} value={saleperson.employee_id}>
                        {saleperson.first_name} {saleperson.last_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={customer} required onChange={handleCustomerChange} name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input value={price} onChange={handlePriceChange} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                <label htmlFor="name">Price</label>
              </div>
              <div>
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
