import React, { useEffect, useState } from 'react';

export default function AppointmentForm() {
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        if (value === '') {
          setDateTime('');
        } else {
          setDateTime(value);
        }
      }
      const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
      }
      const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
      }
      const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
      }
      const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.date_time = dateTime;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const appointment = await fetch(appointmentUrl, fetchConfig);
        if (appointment.ok) {
            const newAppointment = await appointment.json();
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
      }

        return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create An Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment">
                <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" maxLength={17} name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDateTimeChange} value={dateTime} placeholder="dateTime" name="dateTime" id="dateTime" type="datetime-local" className="form-control"></input>
                    <label htmlFor="dateTime">Date & Time</label>
                </div>
                <div className="mb-3">
                <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                <option value="">Technician</option>
                {technicians?.map((technician) => {
                    return (
                    <option key={technician.id} value={technician.id}>{technician.first_name}</option>
                    )
                })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="reason" type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
                </div>
                <button className="main-btn">Create</button>
            </form>
            </div>
        </div>
        </div>
        )
}
