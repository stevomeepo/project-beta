import React, { useEffect, useState } from 'react';

export default function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [query, setQuery] = useState("");

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const handleSearch = () => {
        const searchAppointments = appointments.filter(appointment =>
            appointment.vin.includes(query)
        );
        setAppointments(searchAppointments);
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div>
        <h1>Service History</h1>
        <div className="app">
            <input className="search" placeholder="search" onChange={(e) => setQuery(e.target.value)}/>
            <button onClick={handleSearch}>Search</button>
        </div>
        <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>DateTime</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.vin}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
