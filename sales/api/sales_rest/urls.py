from django.urls import path
from .views import (
    api_list_salespeople,
    api_salespeople,
    api_list_customer,
    api_customer,
    api_list_sales,
    api_sales,
)


urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", api_salespeople, name="api_salespeople"),
    path("customers/", api_list_customer, name="api_list_customer"),
    path("customers/<int:id>/", api_customer, name="api_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", api_sales, name="api_sales"),
]
